#include <TimerOne.h>
#include "motor.h"

const uint16_t pulses_per_revolution = 32;

const uint8_t pin_led = 13;
const uint8_t pin_A = 12;
const uint8_t pin_B = 10;
const uint8_t pin_analogue = A0;

const uint16_t adc_max = (1<<10);
const float loop_time = 0.0015;  // Time for on cycle in the loop. Used the led to measure this.

// Motor parameters
const float R_A = 0.4;    // Terminal resistance [Ohm]
const float L_A = 0.05;   // Terminal inductance [Henry]
const float K_T = 0.5;    // Torque constant [Nm/A]
const float K_EMF = 1.25; // Electromotive force (back EMF) constant [V/rad/s]
const float B_M = 0.01;   // Friction [Nm/rad/s]
const float J_M = 0.5;    // Angular momentum [kg*m**2]

// Other definition that should not be required to be changed
const float tau = 2*M_PI;
const uint16_t pulse_changes_per_revolution = 4*pulses_per_revolution;
const uint32_t one_s = 1000000ul;

Motor motor(R_A, L_A, K_T, K_EMF, J_M, B_M);
bool direction_reverse = true;


void setup() {
  pinMode(pin_led, OUTPUT);
  pinMode(pin_A, OUTPUT);
  pinMode(pin_B, OUTPUT);

  Timer1.initialize(one_s);
  Timer1.attachInterrupt(generated_ab_pulse);

  motor.load.friction = 1.3;
  motor.load.inertia = 0.5;
}

/**
 * The arduino is really maxed out, and on top of that, since the timer is started, stopped and 
 * reconfigured constantly, there is not way to measure the real time correct. Luckyly, the time
 * it take to run the loop once is quite consistant - and probably good enough for the motor 
 * simulation. 
 * 
 * The first thing the loop does, in to toggle the led. This way it is possible to measure the 
 * time it takes to loop to run. On my UNO running at 16 MHz it time for the loop was 1.5 ms. If
 * it is required to change the step time for the simulation, this can be done using the global 
 * constant `loop_time`.
 */
void loop() {
  digitalWrite(pin_led, !digitalRead(pin_led));
  
  motor.supply.voltage = control_voltage();
  motor.step(loop_time);
  float angular_velocity = motor.angular_velocity(); 

  // Handle direction
  if(angular_velocity > 0) {
    direction_reverse = false;
  }
  else {
    direction_reverse = true;
    angular_velocity = -1*angular_velocity;
  }

  update_ab_timer(angular_velocity);
}

float control_voltage() {
  int adc = analogRead(A0);
  return map(adc, 0, adc_max, -10, 20);
}

void update_ab_timer(float angular_velocity) {
  if(angular_velocity < 0.1) {
    Timer1.stop();
  }
  else {
    float dt_s = time_betwee_pulse_change(angular_velocity, pulse_changes_per_revolution);
    int32_t dt = to_us(dt_s);
   
    Timer1.setPeriod(dt);
    Timer1.resume();
  }
}

uint8_t counter = 0;
void generated_ab_pulse() {
  ++counter;
  const uint8_t tp = counter ^ (direction_reverse ? 0xff : 0) ;
  
  switch(tp & 0b11) {
    case 0b00:
      digitalWrite(pin_A, HIGH);
      break;
    case 0b01:
      digitalWrite(pin_B, HIGH);
      break;
    case 0b10:
      digitalWrite(pin_A, LOW);
      break;
    case 0b11:
      digitalWrite(pin_B, LOW);
      break;
  }
}

int32_t to_us(const float s) {
  return 1000000.0*s;
}

/**
 * Note the pulse change time is one 4th of the signal period time.
 * 
 * @parm angular_velocity
 * @param pulse_changes_per_revolution is the number of interrupts per revolution, which is 
 *        4 times the number of pulses pr channel pr revolution.
 * 
 * I:  * * * * * * * * * * * * * * * * * * *
 * A: ---+   +---+   +---+   +---+   +---+
 *       +---+   +---+   +---+   +---+
 *
 * B:  +---+   +---+   +---+   +---+   +---+
 *    -+   +---+   +---+   +---+   +---+
 */
float time_betwee_pulse_change(float angular_velocity, int pulse_changes_per_revolution) {
  float time_per_rev = tau/angular_velocity;
  return time_per_rev/pulse_changes_per_revolution;
}
