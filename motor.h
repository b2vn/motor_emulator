
struct Supply {
  float amperage = 0;
  float voltage = 0;
};

struct Load {
  float inertia = 0;
  float friction = 0;
};

class Motor {
  protected:
    float ia = 0;
    float ib = 0;

    float ra;
    float la; 
    float kt; 
    float kemf;
    float jm;
    float bm;
    
  public:
    Load load;
    Supply supply;
  
    /*
    DC motor simulator. 
    
    ra   : Armature resistance [Ohm]
    la   : Armature inductance [Henry]
    kt   : Torque constant [Nm/A]
    kemf : Electromotive force (back EMF) constant [V/rad/s]
    jm   : Motor angular momentum [kg*m**2]
    bm   : Motor friction [Nm/rad/s]
    
    */
    Motor(float ra, float la, float kt, float kemf, float jm, float bm) 
        : ra(ra), la(la), kt(kt), kemf(kemf), jm(jm), bm(bm) {
    }

    float torque() {
        return kt * ia;
    }
    
    float angular_velocity(){
        return ib;
    }
    
    void step(float dt) {
        step_environment(dt);
        step_motor(dt);
    }

protected:
    void step_motor(float dt) {
        // "Correct" the input voltage for the current load aka "back EMF"
        float v = supply.voltage - kemf*ib;
        float l = la;
        float r = ra;
        
        float e = exp(-r/l*dt);
        ia = ia*e + v/r*(1-e);
    }

    void step_environment(float dt) {
        float v = torque();
        float l = jm + load.inertia;
        float r = bm + load.friction;
                   
        float e = exp(-r/l*dt);          
        ib = ib*e + v/r*(1-e);
    }
};
