# Motor emulator

An Arduino project for simulating a DC motor. 

## Dependencies

* Build with Arduino 1.8.12
* Uses TimerOne

## Python simulation

The motor parameters can be validated with the Jupyter `motor_simulator.ipynb`.

